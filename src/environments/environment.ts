// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    nutrino: {
        apiKey: '4a39edf4a39edfc8146a3f0422f887e0e63804dd',
        nutrinoId: 'ronen_miller',
        apiUrl: 'https://api.nutrino.co/v4/',
        imagesApiUrl: 'https://images.nutrino.co/food/',
        scaledImagesApiUrl: 'https://images.nutrino.co/food/resizescale/'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
