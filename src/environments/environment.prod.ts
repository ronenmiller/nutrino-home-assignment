export const environment = {
    production: true,
    nutrino: {
        apiKey: '4a39edf4a39edfc8146a3f0422f887e0e63804dd',
        nutrinoId: 'ronen_miller',
        apiUrl: 'https://api.nutrino.co/v4/',
        imagesApiUrl: 'https://images.nutrino.co/food/',
        scaledImagesApiUrl: 'https://images.nutrino.co/food/resizescale/'
    }
};
