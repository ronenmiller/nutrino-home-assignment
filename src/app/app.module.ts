import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {SelectedFoodsListComponent} from './components/selected-foods-list/selected-foods-list.component';
import {HomePageComponent} from './components/home-page/home-page.component';
import {FoodItemsLoggerComponent} from './components/food-items-logger/food-items-logger.component';
import {FlexModule} from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {FoodItemComponent} from './components/food-item/food-item.component';

@NgModule({
    declarations: [
        AppComponent,
        SelectedFoodsListComponent,
        HomePageComponent,
        FoodItemsLoggerComponent,
        FoodItemComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MaterialModule,
        FlexModule,
        FormsModule,
        HttpClientModule,
        ScrollingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
