import {Component, OnInit} from '@angular/core';
import {IFoodItem} from '../../models/food-item.model';
import {FoodService} from '../../services/food.service';

@Component({
    selector: 'ntrno-selected-foods-list',
    templateUrl: './selected-foods-list.component.html',
    styleUrls: ['./selected-foods-list.component.scss']
})
export class SelectedFoodsListComponent implements OnInit {
    selectedItems: IFoodItem[] = [];

    constructor(private food: FoodService) {
        this.food.selectedItemsMap$.subscribe(map => {
            if (!map) {
                return;
            }
            this.selectedItems = Array.from(map.values());
        });
    }

    ngOnInit() {
    }

}
