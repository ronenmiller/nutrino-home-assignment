import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {IFoodItem} from '../../models/food-item.model';
import {FoodService} from '../../services/food.service';

@Component({
    selector: 'ntrno-food-item',
    templateUrl: './food-item.component.html',
    styleUrls: ['./food-item.component.scss']
})
export class FoodItemComponent implements OnInit {
    @Input() item: IFoodItem;
    @Input() selected: boolean;

    constructor(private food: FoodService) {
    }

    ngOnInit() {
    }

    getImageUrl() {
        return this.food.getItemThumbnailUrl(this.item);
    }
}
