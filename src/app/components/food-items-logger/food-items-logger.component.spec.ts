import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodItemsLoggerComponent } from './food-items-logger.component';

describe('FoodItemsLoggerComponent', () => {
  let component: FoodItemsLoggerComponent;
  let fixture: ComponentFixture<FoodItemsLoggerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodItemsLoggerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodItemsLoggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
