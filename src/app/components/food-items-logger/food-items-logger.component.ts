import {Component, OnInit, ViewChild} from '@angular/core';
import {FoodService} from '../../services/food.service';
import {BehaviorSubject} from 'rxjs';
import {IFoodItem} from '../../models/food-item.model';
import {CdkVirtualScrollViewport} from '@angular/cdk/scrolling';

@Component({
    selector: 'ntrno-food-items-logger',
    templateUrl: './food-items-logger.component.html',
    styleUrls: ['./food-items-logger.component.scss']
})
export class FoodItemsLoggerComponent implements OnInit {

    private searchTimeout: any;
    private lastFetchedOffset = 0;
    typing: boolean;
    searchText: string;
    currentFoodItem$: BehaviorSubject<IFoodItem>;
    searchFoodItems$ = new BehaviorSubject<IFoodItem[]>(null);
    minimizeHeader = false;
    previewImageLoaded: boolean;
    selectedItemsMap$: BehaviorSubject<Map<string, IFoodItem>>;
    @ViewChild('virtualScrollList') virtualScrollList: CdkVirtualScrollViewport;

    constructor(private food: FoodService) {
        this.currentFoodItem$ = this.food.currentFoodItem$;
        this.onSearchChange(false);
    }

    ngOnInit() {
        this.food.searchFoodItems$.subscribe((items) => {
            if (this.virtualScrollList && this.lastFetchedOffset === 0) {
                this.virtualScrollList.scrollToOffset(0);
            }
            this.searchFoodItems$.next(items);
        });

        this.selectedItemsMap$ = this.food.selectedItemsMap$;
    }

    onSearchChange(shouldWait: boolean = true) {
        if (this.searchTimeout) {
            clearTimeout(this.searchTimeout);
        }
        if (shouldWait) {
            setTimeout(() => {
                this.lastFetchedOffset = 0;
                this.food.updateSearchFoodItems(this.searchText);
            }, 500);
        } else {
            this.food.updateSearchFoodItems(this.searchText);
        }
    }

    onItemSelect(item: IFoodItem) {
        if (this.currentFoodItem$.value && (item.foodId !== this.currentFoodItem$.value.foodId)) {
            this.previewImageLoaded = false;
        }
        this.food.setCurrentFoodItem(item);
        this.food.toggleSelectItem(item);
    }

    onListScroll() {
        if (!this.searchFoodItems$.value) {
            return;
        }
        const lastRenderedIndex = this.virtualScrollList.getRenderedRange().end;
        const listLength = this.searchFoodItems$.value.length;

        // We take a margin of 20 results then fetch the next batch, in order to make the scrolling experience smoother.
        if ((listLength - lastRenderedIndex) <= 20 && this.lastFetchedOffset !== listLength) {
            this.lastFetchedOffset = this.searchFoodItems$.value.length;
            this.food.updateSearchFoodItems(this.searchText, this.lastFetchedOffset);
        }

        this.checkForHeaderResize();
    }

    checkForHeaderResize() {
        this.minimizeHeader = this.virtualScrollList.getRenderedRange().start > 3;
    }

    getCurrentItemPreviewImageUrl() {
        return this.food.getCurrentItemPreviewImageUrl();
    }
}
