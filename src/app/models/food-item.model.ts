export interface IFoodItem {
    displayName: string;
    foodId: string;
    images: string[];
}

export class FoodItem implements IFoodItem {
    displayName: string;
    foodId: string;
    images: string[];

    constructor(item: IFoodItem) {
        this.displayName = item.displayName;
        this.foodId = item.foodId;
        this.images = item.images;
    }
}
