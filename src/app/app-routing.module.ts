import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomePageComponent} from './components/home-page/home-page.component';
import {FoodItemsLoggerComponent} from './components/food-items-logger/food-items-logger.component';

const routes: Routes = [
    {path: '', component: HomePageComponent},
    {path: 'logger', component: FoodItemsLoggerComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
