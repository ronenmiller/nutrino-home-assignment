import {MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule, MatListModule} from '@angular/material';
import {NgModule} from '@angular/core';

@NgModule({
    imports: [MatButtonModule, MatCheckboxModule, MatListModule, MatFormFieldModule, MatIconModule, MatInputModule],
    exports: [MatButtonModule, MatCheckboxModule, MatListModule, MatFormFieldModule, MatIconModule, MatInputModule],
})
export class MaterialModule {
}
