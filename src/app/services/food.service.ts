import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {ApiService} from './api.service';
import {IFoodItem} from '../models/food-item.model';
import {StorageService} from './storage.service';

@Injectable({
    providedIn: 'root'
})
export class FoodService {

    searchFoodItems$ = new BehaviorSubject<IFoodItem[]>(null);
    currentFoodItem$ = new BehaviorSubject<IFoodItem>(null);
    selectedItemsMap$ = new BehaviorSubject<Map<string, IFoodItem>>(null);
    selectedItemsMap: Map<string, IFoodItem>;

    constructor(private api: ApiService) {
        this.selectedItemsMap = StorageService.loadSelectedItemsMap();
        this.selectedItemsMap$.next(this.selectedItemsMap);
    }

    updateSearchFoodItems(text: string, offset = 0) {
        this.api.getFoodItems(text, 25, offset).subscribe(
            (results: IFoodItem[]) => {
                if (offset === 0) {
                    this.searchFoodItems$.next(results);
                } else {
                    this.searchFoodItems$.next([...this.searchFoodItems$.value, ...results]);
                }
            });
    }

    getScaledItemImage(url: string, width: number = 150, height: number = 150) {
        return this.api.getResizeScaledImageUrl(url, width, height);
    }

    setCurrentFoodItem(item: IFoodItem) {
        this.currentFoodItem$.next(item);
    }

    getCurrentItemPreviewImageUrl() {
        if (!this.currentFoodItem$.value) {
            return null;
        }
        const currentFoodItem = this.currentFoodItem$.value;
        return currentFoodItem.images.length > 0 ? this.getScaledItemImage(currentFoodItem.images[0]) :
            'https://via.placeholder.com/150.png?text=No+Image';
    }

    getItemThumbnailUrl(item: IFoodItem) {
        return item.images.length > 0 ? this.getScaledItemImage(item.images[0], 40, 40) :
            'https://via.placeholder.com/40.png?text=No+Image';
    }

    toggleSelectItem(item: IFoodItem) {
        if (this.selectedItemsMap.has(item.foodId)) {
            this.selectedItemsMap.delete(item.foodId);
        } else {
            this.selectedItemsMap.set(item.foodId, item);
        }
        this.selectedItemsMap$.next(this.selectedItemsMap);
        StorageService.storeSelectedItemsMap(this.selectedItemsMap);
    }
}
