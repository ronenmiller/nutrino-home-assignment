import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpEvent, HttpHeaders, HttpResponse} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {FoodItem, IFoodItem} from '../models/food-item.model';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    httpOptions: any;

    constructor(private http: HttpClient) {
        // Initialize the required http headers
        const headers = {
            'Content-Type': 'application/json',
            'x-api-key': environment.nutrino.apiKey
        };

        this.httpOptions = {
            headers: new HttpHeaders(headers)
        };
    }

    /**
     * Build the search request body data
     */
    static buildSearchRequestData(text: string, size: number, offset: number) {
        return {
            nutrinoId: environment.nutrino.nutrinoId,
            text,
            size,
            offset
        };
    }

    /**
     * Post request to server to get food items by search text
     * @param text - the text to search
     * @param numItems - the number of items to fetch from the server
     * @param offset - the item index from which we start fetching from the search results
     */
    getFoodItems(text: string, numItems: number, offset: number): Observable<IFoodItem[]> {
        return this.http.post<IFoodItem[]>(environment.nutrino.apiUrl + 'fooditems/_search',
            ApiService.buildSearchRequestData(text, numItems, offset),
            // We use observe response and then map the results for Typescript to be able to infer the correct types
            {...this.httpOptions, observe: 'response'})
            .pipe(map((response: HttpResponse<IFoodItem[]>) => response.body.map(item => new FoodItem(item))))
            .pipe(catchError(this.handleError));
    }

    /**
     * Get a scaled image from an original image URL
     * @param srcImageUrl - the original image url
     * @param width - desired width
     * @param height - desired height
     */
    getResizeScaledImageUrl(srcImageUrl: string, width: number | string, height: number | string) {
        const imageName = srcImageUrl.replace(environment.nutrino.imagesApiUrl, '');
        return environment.nutrino.scaledImagesApiUrl + `${width}` + `/${height}/` + imageName;
    }

    /**
     * Handle errors from API
     * @param error The error object passed to the handler
     */
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${JSON.stringify(error.error)}`
            );
        }
        // return an observable with a user-facing error message
        return throwError('Server API error. Please see console log.');
    }

}
