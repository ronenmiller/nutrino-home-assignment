import {Injectable} from '@angular/core';
import {IFoodItem} from '../models/food-item.model';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor() {
    }

    static storeSelectedItemsMap(map: Map<string, IFoodItem>) {
        localStorage.setItem('selectedItems', JSON.stringify(Array.from(map.entries())));
    }

    static loadSelectedItemsMap(): Map<string, IFoodItem> {
        return new Map<string, IFoodItem>(JSON.parse(localStorage.getItem('selectedItems')));
    }
}
