import { Component } from '@angular/core';

@Component({
  selector: 'ntrno-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'nutrino';
}
